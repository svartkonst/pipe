/**
 * `Pipe` is a wrapper that lets you chain transformations. 
 * 
 * The two basic operations are `thru(fn, ...args)`, which maps the value and returns a new Pipe, 
 * and `unwrap()`, which returns the contained value. 
 * 
 * Similar to how pipes work in Elixir, `thru()` can take a lambda, but also 
 * allows for a point-free style without requiring curried functions. 
 * 
 * UNFORTUNATELY, so far, this does not work with functions that are generic and infer args.
 * 
 * The example demonstrates both variants.
 * 
 * @example
 *   const set = (source, key, value) => { ...source, [key]: value }
 *   
 *   const mobyDict = pipe({ caught: 0, spotted: 1 })
 *      .thru(src => set(src, 'spotted', 2)) 
 *      .thru(set, 'caught', 2)
 *      .unwrap() // = { caught: 2, spotted: 2 }
 * 
 * @example 
 *   // Generic, inferred function. This will not work:
 *   function set<T, K extends keyof T>(source: T, key: K, value: T[K]) {
 *     return { ...source, [key]: value };   
 *   } 
 * 
 *   pipe({ caught: 0, spotted: 1 })
 *     .thru(set, 'caught', 1);
 */
export class Pipe<T> {
  constructor(public value: T) { }

  thru<F extends Fn>(fn: F, ...args: Rest<F>): Pipe<ReturnType<F>> {
    const transformed = fn.apply(null, [this.value, ...args]);

    return new Pipe(transformed);
  }

  unwrap(): T {
    return this.value;
  }
}

export function pipe<T>(value: T): Pipe<T> {
  return new Pipe(value);
}

type Fn = (...args: [any, ...any[]]) => any;

type Rest<F extends Fn> = Parameters<F> extends [unknown, ... infer Tail]
  ? Tail
  : [];