/**
 * `Pipe` is a wrapper that lets you chain transformations.
 *
 * The two basic operations are `thru(fn, ...args)`, which maps the value and returns a new Pipe,
 * and `unwrap()`, which returns the contained value.
 *
 * Similar to how pipes work in Elixir, `thru()` can take a lambda, but also
 * allows for a point-free style without requiring curried functions.
 *
 * The example demonstrates both variants.
 *
 * @example
 *   const set = (source, key, value) => { ...source, [key]: value }
 *
 *   const mobyDict = pipe({ caught: 0, spotted: 1 })
 *      .thru(src => set(src, 'spotted', 2))
 *      .thru(set, 'caught', 2)
 *      .unwrap() // = { caught: 2, spotted: 2 }
 */
export class Pipe<T> {
    value: T;
    constructor(value: T);
    thru<U>(): Pipe<U>;
    unwrap(): T;
}
export function pipe<T>(value: T): Pipe<T>;

//# sourceMappingURL=types.d.ts.map
