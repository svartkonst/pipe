function $parcel$exportWildcard(dest, source) {
  Object.keys(source).forEach(function(key) {
    if (key === 'default' || key === '__esModule' || dest.hasOwnProperty(key)) {
      return;
    }

    Object.defineProperty(dest, key, {
      enumerable: true,
      get: function get() {
        return source[key];
      }
    });
  });

  return dest;
}
function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}
var $5def3b0429031658$exports = {};

$parcel$export($5def3b0429031658$exports, "Pipe", () => $5def3b0429031658$export$c6d5e58b58964667);
$parcel$export($5def3b0429031658$exports, "pipe", () => $5def3b0429031658$export$a4627e546088548d);
class $5def3b0429031658$export$c6d5e58b58964667 {
    constructor(value){
        this.value = value;
    }
    thru() {}
    unwrap() {
        return this.value;
    }
}
function $5def3b0429031658$export$a4627e546088548d(value) {
    return new $5def3b0429031658$export$c6d5e58b58964667(value);
}


$parcel$exportWildcard(module.exports, $5def3b0429031658$exports);


//# sourceMappingURL=index.js.map
